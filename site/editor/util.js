export const download = (filename, mime, text) => {
  const a = html('a', {
    href: `data:${mime};charset=utf-8,${encodeURIComponent(text)}`,
    download: filename,
  });
  a.style.display = 'none';
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

export const html = (tag, attrs, kids) => {
  let el = document.createElement(tag);
  el.append = kid => {
    if (typeof kid === 'string')
      el.appendChild(document.createTextNode(kid));
    else if (kid instanceof Array)
      for (let k of kid) el.append(k);
    else if (kid === null || kid === undefined)
      return
    else
      el.appendChild(kid);
    return el
  }
  for (let attr in attrs)
    if (attr === 'klass' || attr === '_class') 
      el.setAttribute('class', attrs[attr]);
    else
      el.setAttributeNS(null, attr, attrs[attr]);
  if (kids)
    for (let kid of kids)
      el.append(kid);
  return el
};

export const svg = (tag, attrs, kids) => {
  const ns = 'http://www.w3.org/2000/svg';
  const el = (typeof tag == 'string') ? document.createElementNS(ns, tag) : tag;
  el.append = kid => {
    if (typeof kid === 'string')
      el.appendChild(document.createTextNode(kid));
    else if (kid instanceof Array)
      for (let k of kid) el.append(k);
    else if (kid === null || kid === undefined)
      return
    else
      el.appendChild(kid);
    return el
  }
  for (let attr in attrs) {
    if (attr === 'klass' || attr === '_class') 
      el.setAttributeNS(null, 'class', attrs[attr]);
    else
      el.setAttributeNS(null, attr, attrs[attr]);
  }
  if (kids) for (let kid of kids) el.append(kid);
  return el;
};


export function join(parent, tag, data, lifecycle){
  let kids = parent.querySelectorAll(tag);
  let max = Math.max(data.length, kids.length);
  for (let i=0; i<max; i++) {
    //
    //no data, so remove the element
    if (data.length < i) {
      if (lifecycle.teardown) lifecycle.teardown(el[i]);
      el[i].remove()
    // 
    //data but no element, so create, append, and update the kid
    } else if (kids.length < i) {
      let el = lifecycle.create ? lifecycle.create() : document.createElement(tag);
      parend.appendChild(el);
      lifecycle.update(el, data[i], i);
    // 
    //kids and data exist, so update
    } else {
      lifecycle.update(kids[i], data[i]);
    }
  }
}

//
// fillRange(5, 15, ["5.1", "5.2", "5.3"])
// 
// returns
// 
// [{"grade": "5.1", "start": 5, "stop": 8},
//  {"grade": "5.2", "start": 9, "stop": 12},
//  {"grade": "5.3", "start": 13, "stop": 15}]
//
export function fillRange(start, stop, grades){
  if (grades.length === 0) return [];
  // there's an empty segment between each grade
  let gaps = grades.length - 1;
  let unassigned = stop - start - gaps;
  // 
  // make a "bucket" for each value
  // and fill the buckets with segments (from left to right)
  // 
  let buckets = grades.map(n => 0);
  let i = 0;
  while (unassigned > 0) {
    unassigned -= 1;
    buckets[i] += 1;
    if ((i+1) == buckets.length) { // if the next bucket index doesn't exist
      i = 0;
    } else {
      i += 1;
    }
  }
  let cursor = start;
  for (let bucket of buckets) {
    if (bucket === 0) return null;
  }
  return grades.map((v, i) => {
      let a = cursor;
      let b = buckets[i] + cursor;
      cursor = b+1;
      return {grade: v, start: a, stop: b};
  });
}

