
const toHomepageHtml = function(table){
  let lines = ['<ul class="scales">'];
  for (let scale of table.listScales()) {
    lines.push(`  <li class="scale">`)
    lines.push(`    <span class="scale-name">${scale}</span>`)
    lines.push(`    <ul class="series-list">`)
    for (let series of table.listSeriesByScale(scale)) {
      lines.push(`      <li class="series">`)
      lines.push(`        <span class="series-name">${series}</span>`)
      lines.push(`        <ol class="series-grades">`)
      table.forEachGradeBySeries(scale, series, (grade, lower, upper) => {
        lines.push(`          <li class="series-grade" data-series=${series} data-lower=${lower} data-upper=${upper}>${grade}</li>`)
      });
      lines.push(`        </ol>`)
      lines.push(`      </li>`)
    }
    lines.push(`    </ul>`)
    lines.push(`  </li>`)
  }
  lines.push(`</ul>`)
  return lines.join('\n');
};

const toHtmlLists = function(table){
  let lines = ['<ul class="scales">'];
  for (let scale of table.listScales()) {
    lines.push(`  <li class="scale">`)
    lines.push(`    <span class="scale-name">${scale}</span>`)
    lines.push(`    <ol class="grades">`)
    for (let series of table.listSeriesByScale(scale)) {
      table.forEachGradeBySeries(scale, series, (grade, lower, upper) => {
        lines.push(`      <li class="grade" data-series=${series} data-lower=${lower} data-upper=${upper}>${grade}</li>`)
      });
    }
    lines.push(`    </ol>`)
    lines.push(`  </li>`)
  }
  lines.push(`</ul>`)
  return lines.join('\n');
}

