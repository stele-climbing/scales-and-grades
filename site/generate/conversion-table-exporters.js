
export class Table {
    constructor(data){
        this.d           = {};
        this.scaleOrder  = [];
        this.seriesOrder = {};
        if (typeof data === 'object') {
            this.addJsonLikeObject(data);
        } else {
            throw Error('unsupported input')
        }
    }

    /*
     *  relatively forgiving.
     *  just ensure that the requisite parents exist before adding the grade
     */
    addGrade(scale, series, grade, start, stop) {
        if (!(scale  in this.d))        this.d[scale]  = {};
        if (!(series in this.d[scale])) this.d[scale][series] = [];
        this.d[scale][series].push({grade, start: Number(start), stop: Number(stop)});
        this.addToSorting(scale, series);
    }

    addToSorting(scaleName, seriesName){
        if (!this.scaleOrder.includes(scaleName)) {
            this.scaleOrder.push(scaleName);
            this.seriesOrder[scaleName] = [];
        }
        if (!this.seriesOrder[scaleName].includes(seriesName)) {
            this.seriesOrder[scaleName].push(seriesName);
        }
    }

    /*
     *  The JSON-like object looks like our standard json export
     *
     *  {
     *    French: {
     *      standard: [{grade: "2",start: 0,stop: 23} ...]
     *    }
     *    "Yosemite Decimal System": {
     *      standard: [{grade: "5.0",   start: 0, stop: 7},
     *                 {grade: "5.1",   start:8,  stop:15}  ...more]
     *      abcd:     [{grade: "5.10a", start: 0, stop: 6},
     *                 {grade: "5.1",   start: 7, stop:13}  ...more]
     *    },
     *  }
     *
     */
    addJsonLikeObject(o){
        for (let scaleName in o) {
            for (let seriesName in o[scaleName]) {
                for (let {grade,start,stop} of o[scaleName][seriesName]) {
                    this.addGrade(scaleName, seriesName, grade, start, stop);
                }
            }
        }
    }

    eachGrade(f){
        for (let scaleName in this.d) {
            for (let seriesName in this.d[scaleName]) {
                for (let {grade,start,stop} of this.d[scaleName][seriesName]) {
                    f({scale: scaleName, series: seriesName,
                       seriesIndex: this.getSeriesIndex(scaleName, seriesName),
                       grade, start, stop});
                }
            }
        }
    }

    getGrades(){
        let a = [];
        for (let scaleName in this.d) {
            for (let seriesName in this.d[scaleName]) {
                for (let {grade,start,stop} of this.d[scaleName][seriesName]) {
                    a.push({
                        scale: scaleName, series: seriesName,
                        seriesIndex: this.getSeriesIndex(scaleName, seriesName),
                        grade, start, stop});
                }
            }
        }
        return a;
    }

    /*
     * Series and Scales are both ordered.
     *
     * |             YDS             |  French  |
     * |-----------------------------|----------|
     * | standard | plusMinus | abcd | standard |
     * |----------|-----------|------|----------|
     * |    0     |     1     |  2   |    3     |
     *
     */
    getSeriesIndices(){
        let a = [];
        let i = 0;
        for (let scale of this.scaleOrder) {
            for (let series of this.seriesOrder[scale]) {
                a.push({scale, series, i});
                i += 1;
            }
        }
        return a;
    }

    getTotalNumberOfSeries(){
        return this.getSeriesIndices().length;
    }

    getSeriesIndex(scale, series){
        return this.getSeriesIndices().find(v => {
            return (v.scale === scale) && (v.series === series);
        }).i;
    }

    getLowestStart(){
        const starts = this.getGrades().map(g => g.start);
        return starts.reduce((accum, v) => Math.min(accum, v));
    }

    getHighestStop(){
        const stops = this.getGrades().map(g => g.stop);
        return stops.reduce((accum, v) => Math.max(accum, v));
    }
}

