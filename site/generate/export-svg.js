
export class Config {
    constructor(){
        this.incrementLength       = 6;
        this.gradeWidth            = 60;
        this.seriesSpacing         = 10;
        this.gradeBackgroundColor  = '#703A39';
        this.gradeBorderColor      = '#97D983';
        this.gradeBorderWidth      = 2;
        this.gradeFontSize         = 13;
        this.gradeFontFamily       = 'sans';
        this.gradeFontColor        = '#9570A6';
        // backgroundColor
        // padding
        // gradeRadius
   }
}

const input = (typ, attrs) => {
    const el = document.createElement('input');
    el.setAttribute('type', typ);
    for (let [k, v] of Object.entries(attrs))
        el.setAttribute(k, v);
    return el;
}

export class ConfigEditor {
    constructor(){
        this.createElement();
    }
    createElement(){
        this.inputIncrementLength = input('number', {
            min: 0,
            placeholder: 'increment length',
        });
        this.inputGradeWidth = input('number', {
            min: 2,
            placeholder: 'grade width',
        });
        this.inputSeriesSpacing = input('number', {
            min: 0,
            placeholder: 'series spacing',
        });
        this.inputGradeBackgroundColor = input('color', {
            placeholder: 'grade background color',
        });
        this.inputGradeBorderColor = input('color', {
            placeholder: 'grade border color',
        });
        this.inputGradeBorderWidth = input('number', {
            placeholder: 'grade border width',
        });
        this.inputGradeFontSize = input('number', {
            placeholder: 'grade font size',
        });
        this.inputGradeFontFamily = input('text', {
            placeholder: 'grade font family',
        });
        this.inputGradeFontColor = input('color', {
            placeholder: 'grade font color',
        });
        this.container = document.createElement('div');
        this.container.classList.add('config');
        [this.inputIncrementLength,
         this.inputGradeWidth,
         this.inputSeriesSpacing,
         this.inputGradeBackgroundColor,
         this.inputGradeBorderColor,
         this.inputGradeBorderWidth,
         this.inputGradeFontSize,
         this.inputGradeFontFamily,
         this.inputGradeFontColor].forEach(el => {
            el.addEventListener('change', () => this.fireUpdate());
            const id = 'export_svg_' + (Math.random() + 1).toString(36).substring(7);
            const div = document.createElement('div');
            const label = document.createElement('label');
            label.setAttribute('for', id);
            label.textContent = el.getAttribute('placeholder');
            el.setAttribute('id', id);
            el.setAttribute('name', id);
            div.appendChild(label);
            div.appendChild(el);
            this.container.appendChild(div);
        });
    }
    fireUpdate(){
        this.onupdate(this.getValue());
    }
    getValue(){
        console.warn('TODO: return a real Config object')
        return {
            incrementLength:      Number(this.inputIncrementLength.value),
            gradeWidth:           Number(this.inputGradeWidth.value),
            seriesSpacing:        Number(this.inputSeriesSpacing.value),
            gradeBackgroundColor: this.inputGradeBackgroundColor.value,
            gradeBorderColor:     this.inputGradeBorderColor.value,
            gradeBorderWidth:     Number(this.inputGradeBorderWidth.value),
            gradeFontSize:        Number(this.inputGradeFontSize.value),
            gradeFontFamily:      this.inputGradeFontFamily.value,
            gradeFontColor:       this.inputGradeFontColor.value,
        }
    }
    update(cfg){
        this.inputIncrementLength.value      = cfg.incrementLength;
        this.inputGradeWidth.value           = cfg.gradeWidth;
        this.inputSeriesSpacing.value        = cfg.seriesSpacing;
        this.inputGradeBackgroundColor.value = cfg.gradeBackgroundColor;
        this.inputGradeBorderColor.value     = cfg.gradeBorderColor;
        this.inputGradeBorderWidth.value     = cfg.gradeBorderWidth;
        this.inputGradeFontSize.value        = cfg.gradeFontSize;
        this.inputGradeFontFamily.value      = cfg.gradeFontFamily;
        this.inputGradeFontColor.value       = cfg.gradeFontColor;
    }
}

export class Output {
    constructor(){
        this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    }
    buildGradeElement(){
        const g    = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        const txt  = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        g.appendChild(rect);
        g.appendChild(txt);
        return g;
    }
    styleGradeElement(table, element, g, cfg){

        const x1   = g.seriesIndex * (cfg.seriesSpacing + cfg.gradeWidth);
        const x2   = x1 + cfg.gradeWidth;
        const y1   = g.start * cfg.incrementLength;
        const y2   = g.stop  * cfg.incrementLength;

        const rect = element.querySelector('rect');
        rect.setAttributeNS(null, 'x', x1);
        rect.setAttributeNS(null, 'y', y1);
        rect.setAttributeNS(null, 'width', x2 - x1);
        rect.setAttributeNS(null, 'height', y2 - y1);
        rect.style.strokeWidth = cfg.gradeBorderWidth;
        rect.style.stroke      = cfg.gradeBorderColor;
        rect.style.fill        = cfg.gradeBackgroundColor;

        const txt = element.querySelector('text');
        txt.setAttributeNS(null, 'x', (x1+x2)/2);
        txt.setAttributeNS(null, 'y', (y1+y2)/2);

        txt.style.textAnchor       = 'middle';
        txt.style.dominantBaseline = 'central';
        txt.style.fontFamily       = cfg.gradeFontFamily;
        txt.style.fill             = cfg.gradeFontColor;
        txt.style.fontSize         = cfg.gradeFontSize + 'px';

        txt.textContent = g.grade;
        return null;
    }
    redraw(t, cfg){
        this.svg.innerHTML = '';
        const start  = t.getLowestStart();
        const stop   = t.getHighestStop();
        const pad    = cfg.gradeBorderWidth / 2;
        const upper  = (start * cfg.incrementLength) - pad;
        const lower  = (stop  * cfg.incrementLength) + pad;
        const left   = 0 - pad;
        const right  = (t.getTotalNumberOfSeries()*(cfg.gradeWidth+cfg.seriesSpacing)
                        - cfg.seriesSpacing + pad);
        const width  = right - left;
        const height = lower - upper;
        this.svg.setAttributeNS(null, 'viewBox', `${left} ${upper} ${width} ${height}`);
        this.svg.setAttributeNS(null, 'width', width + 'px')
        this.svg.setAttributeNS(null, 'height', height + 'px')
        t.eachGrade(grade => {
            const el = this.buildGradeElement();
            this.styleGradeElement(t, el, grade, cfg);
            this.svg.appendChild(el);
        });

    }
}
